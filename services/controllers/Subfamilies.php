<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Subfamilies extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }
  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['data']);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
      case 'getSubfamiliesByFamId':
        return $this->getSubfamiliesByFamId($_POST['id']);
    }
  }

  /** Obtención de subfamilias por familia */

  public function getSubfamiliesByFamId($id){
    Ws::$c->q("SELECT * FROM subfamilies WHERE sub_deleted = '0' AND sub_fam_id = '$id' ORDER BY sub_id ");
    $subfamilies = array();
    while($subfamily = Ws::$c->fa()){
      $subfamily = $this->xss_client($subfamily);
      array_push($subfamilies, $subfamily);
    }
    return $subfamilies;
  }

  /** Eliminación de una subfamilia */

  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  /** Acceso a una subfamilia única */

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $_SESSION['sub_id'] = $id;
    $single = Ws::$c->fa();
    $single = $this->xss_client($single);
    return $single;
  }

  /** Acceso a la lista de subfamilias */

  public function get(){
    $d = new db();
    $this->sget($d, "", "sub_name");
    $array = array();
    $family = new Families();
    while($row = $d->fa()){
      $row['family'] = $family->single($row['sub_fam_id']);
      $row = $this->xss_client($row);
      array_push($array, $row);
    }
    $d->cl();
    return $array;
  }

  /** Actualización de una subfamilia */

  public function update($data){
    $data = $this->utf8_server($data);
    try {
      $this->upd($_SESSION["sub_id"], $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  /** Creación de una subfamilia */

  public function create($data){
    $data = $this->utf8_server($data);
    try {
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id); 
  }

  public function sql_rules(){
    $this->create_table();
    if (!$this->is_ok($this->foreign_keys, $this->rows)){
      $this->create_fields($this->foreign_keys, 0, true);
      $this->create_fields($this->rows, 0);
    }
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array(
    array('fam_id', 'int(6)', 'NOT NULL')
  );
  /**
   * Set row keys
   */
  public $rows = array(
    array('name', 'varchar(100)', 'NOT NULL'),
    array('deleted', 'int(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>