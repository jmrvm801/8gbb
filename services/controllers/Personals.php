<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Personals extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }
  public function run($method = 'default'){
    switch($method){
      case 'helloworld':
        return $this->helloworld();
      break;
    }
  }

  public function helloworld(){
    return array('shi');
  }

  public function basic($id){
    Ws::$c->q("SELECT per_firstname, per_lastname, per_surname, per_birthday, per_gender FROM personals WHERE per_id = '$id' LIMIT 1;");
    $single = Ws::$c->fa();
    $single = $this->xss_client($single);
    return $this->buildData($single);
  }

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $single = Ws::$c->fa();
    return $this->buildData($single);
  }

  public function buildData($single){
    $single['fullname'] = $single['per_firstname']." ".$single['per_lastname']." ".$single['per_surname'];
    $single['fulllast'] = $single['per_lastname']." ".$single['per_surname']." ".$single['per_firstname'];
    $single['birthday'] = Ws::$g->numberDateToString($single['per_birthday']);
    $single['adjetives'] = array(
      'adj_1' => $single['per_gender'] == '1' ? 'la' : 'él',
      'adj_2' => $single['per_gender'] == '1' ? 'a' : '',
      'adj_3' => $single['per_gender'] == '1' ? 'a' : 'o',
      'adj_4' => $single['per_gender'] == '1' ? 'Femenino' : 'Masculino',
    );
    return $single;
  }

  public function update($per_id, $data){
    try {
      $this->upd($per_id, $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  public function create($data){
    try {
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function delete($id, $physical = false){
    $this->remove($id);
    return Gral::response('true');
  }

  public function sql_rules(){
    $this->create_table();
    if (!$this->is_ok($this->foreign_keys, $this->rows)){
      $this->create_fields($this->foreign_keys, 0, true);
      $this->create_fields($this->rows, 0);
    }
  }

  /**
   * row data
   */
  public $foreign_keys = array();
  /**
   * row data
   */
  public $rows = array(
    array('social_reason', 'varchar(200)', 'NOT NULL'),
    array('firstname', 'varchar(100)', 'NOT NULL'),
    array('lastname', 'varchar(100)', 'NOT NULL'),
    array('surname', 'varchar(100)', 'NOT NULL'),
    array('email', 'varchar(150)', 'NOT NULL'),
    array('birthday', 'date', 'NOT NULL'),
    array('gender', 'int(1)', 'NOT NULL'),
    array('phone', 'varchar(20)', 'NOT NULL'),
    array('deleted', 'int(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>