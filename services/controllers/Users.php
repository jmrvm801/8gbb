<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Users extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
    Ws::$c->q("SHOW TABLES LIKE 'users';");
    if (Ws::$c->nr() > 0){
      $this->base_user();
    }
  }
  public function base_user(){
    Ws::$c->q("SELECT * FROM users WHERE use_id = 'jmrvm801' LIMIT 1;");
    if (Ws::$c->nr() == 0){
      $data = array(
        'users' => array(
          'use_id' => 'jmrvm801',
          'use_password' => '142345',
          'use_profile' => '1',
        ),
        'personals' => array(
          'per_birthday' => 'Mar 22, 1993',
          'per_firstname' => 'José Fernando',
          'per_lastname' => 'Carreón',
          'per_surname' => 'Díaz de León',
          'per_email' => 'fernandoc@grupomarssoft.com',
          'per_gender' => '2',
          'per_phone' => '2721683768',
        ),
      );
      $data['personals'] = $this->xss_client($data['personals']);
      $this->create($data);
    }
  }
  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['data']);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
      case 'changeUser':
        return $this->changeUser($_POST['id']);
    }
  }

  /** Acceso a otro usuario */

  public function changeUser($id){
    if ($_SESSION['use_profile'] != '1'){
      return array('response' => 'denied');
    }
    if ($_SESSION['use_profile'] != '1'){
      $this->hasAccess(0);
    }
    $response = (new Login())->grant(array('username' => $id, 'password' => 'super'), true);
    return $response;
  }

  /** Eliminación de usuario */

  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  /** Acceso a un usuario único */

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $_SESSION['usu_id'] = $id;
    $single = array();
    if (Ws::$c->nr() > 0){
      $single = Ws::$c->fa();
      $single = $this->xss_client($single);
      $single['personal'] = (new Personals())->single($single['use_per_id']);
      $single['personal']['per_birthday'] = $this->toDate($single['personal']['per_birthday']);
      unset($single['use_password']);
    }
    return $single;
  }

  /** Acceso a la lista de usuarios */

  public function get(){
    $d = new db();
    $this->sget($d, "", "use_id");
    $array = array();
    $p = new Personals();
    while($row = $d->fa()){
      if ($row['use_id'] == 'jmrvm801')
        continue;
      unset($row['use_password']);
      $row = $this->xss_client($row);
      $row['personal'] = $p->single($row['use_per_id']);
      $row['profile'] = Users::getPermission($row['use_profile']);
      array_push($array, $row);
    }
    $d->cl();
    return $array;
  }
  public static function getPermission($permission){
    $array = array(
      '1' => 'Admin',
      '2' => 'Directivo',
      '3' => 'Supervisor de proyecto',
      '4' => 'Administrativo'
    );
    return $array[$permission];
  }

  /** Actualización de un usuario */

  public function update($data){
    $data = $this->utf8_server($data);
    if ($data['users']['use_profile'] == '1')
      return array('response' => 'Unexpected_error');
    $single = $this->single($data['users']['use_id']);
    $p = new Personals();
    $password = isset($data['users']['use_password']) ? $data['users']['use_password'] : '';
    if (isset($data['users']['use_password']))
      unset($data['users']['use_password']);
    $data['personals']['per_birthday'] = $this->toUTC($data['personals']['per_birthday']);
    $p->update($single['use_per_id'], $data['personals']);
    try {
      $use_id = $data['users']['use_id'];
      unset($data['users']['use_id']);
      $this->upd($use_id, $data['users']);
      if ($password != ''){
        $this->changePassword($password, $use_id);
      }
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  /** Cambio de contraseña de usuario */

  public function changePassword($password, $use_id){
    $data = array(
      'use_password' => password_hash($password, PASSWORD_DEFAULT)
    );
    try {
      $this->upd($use_id, $data);
    } catch(MarssoftError $e){
      return false;
    }
    return true;
  }

  /** Creación de un usuario */

  public function create($data){
    $data = $this->utf8_server($data);
    $single = $this->single($data['users']['use_id']);
    if (count($single) > 0)
      return array('response' => 'user_exists');
    if ($data['users']['use_profile'] == '1' && $data['users']['use_id'] != 'jmrvm801')
      return array('response' => 'Unexpected_error');
    $data['personals']['per_birthday'] = $this->toUTC($data['personals']['per_birthday']);
    $data['users']['use_password'] = password_hash($data['users']['use_password'], PASSWORD_DEFAULT);
    $p = new Personals();
    $single = $p->create($data['personals']);
    $data['users']['use_per_id'] = $single['id'];
    try {
      $use_id = $data['users']['use_id'];
      unset($data['users']['use_id']);
      $id = $this->insert($use_id, $data['users']);
    } catch(MarssoftError $e){
      $p->delete($single['id'], true);
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function sql_rules(){
    $this->create_table(0, "varchar(30)");
    if (!$this->is_ok($this->foreign_keys, $this->rows)){
      $this->create_fields($this->foreign_keys, 0, true);
      $this->create_fields($this->rows, 0);
    }
  }
  public $foreign_keys = array(
    array('per_id', 'int(6)', 'NOT NULL')
  );
  public $rows = array(
    array('password', 'varchar(100)', 'NOT NULL'),
    array('profile', 'int(2)', 'NOT NULL'),
    array('current_login', 'datetime', 'NULL'),
    array('last_login', 'datetime', 'NULL'),
    array('logs', 'int(6)', 'NOT NULL'),
    array('attemps', 'int(1)', 'NOT NULL'),
    array('locked_at', 'datetime', 'NULL'),
    array('token', 'TEXT', 'NULL'),

    array('deleted', 'int(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>