<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Logs extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }

  public function run($method = 'default'){
    switch ($method) {
      case 'create':
        return $this->create('', $_POST['action'], $_POST['complemento']);
      case 'getLogs':
        return $this->getLogs($_POST['data']);
      case 'loadResources':
        return $this->loadResources();
      case 'getLogsByModule':
        return $this->getLogsByModule($_POST['data']);
    }
  }

  public function getLogsByModule($data){
    Ws::$c->q("SELECT DISTINCT log_method FROM logs WHERE log_class = '$data';");
    $array = array();
    while($row = Ws::$c->fa()){
      $r = (new ReflectionClass($data))->getMethod($row['log_method'])->getDocComment();
      if (!$r)
        $r = 'No disponible';
      else {
        $r = str_replace('/** ', '', $r);
        $r = str_replace(' */', '', $r);
      }

      array_push($array, array(
        'id' => $row['log_method'],
        'value' => $r,
      ));
    }
    return $array;
  }

  public function loadResources(){
    Ws::$c->q("SELECT DISTINCT log_class FROM logs");
    $array = array();
    while($row = Ws::$c->fa()){
      array_push($array, array(
        'id' => $row['log_class'],
        'value' => $row['log_class'],
      ));
    }
    return array(
      'users' => (new Users())->get(),
      'modules' => $array,
    );
  }

  /** Obtiene la lista de eventos del usuario */

  public function getLogs($data){
    $data['ser_start_date'] = date('Y-m-d', strtotime(Ws::$g->stringDateToNumber($data['ser_start_date'])));
    $data['ser_end_date'] = date('Y-m-d', strtotime(Ws::$g->stringDateToNumber($data['ser_end_date'])));

    $q = "SELECT * FROM logs LEFT JOIN users ON log_use_id = use_id LEFT JOIN personals ON use_per_id = per_id WHERE log_deleted = '0'";
    switch($data['use_filter']){
      case '-1':
        break;
      case '0':
        $q .= " AND log_use_id = 'invitado'";
        break;
      default:
        $q .= " AND log_use_id = '$data[use_filter]'";
      break;
    }
    $q .= " AND log_created_at BETWEEN '$data[ser_start_date] 00:00:00' AND '$data[ser_end_date] 23:59:59'";
    if ($data['use_module'] != '0'){
      $q .= " AND log_class = '$data[use_module]'";
    }

    if ($data['use_event'] != '0'){
      $q .= " AND log_method = '$data[use_event]'";
    }

    $q .= " ORDER BY log_id DESC";
    Ws::$c->q($q);
    $clients = array();
    while($client = Ws::$c->fa()){
      unset($client['use_password']);
      $client = $this->xss_client($client);
      $client['fullname'] = $client['per_firstname'].' '.$client['per_lastname'];
      $client['log_browser_info'] = json_decode($client['log_browser_info'], true);
      $client['log_post_data'] = json_decode($client['log_post_data'], true);
      $client['log_post_data'] = $this->solveSecurityIssues($client['log_post_data']);
      $client['log_session_data'] = json_decode($client['log_session_data'], true);
      $client['log_session_data'] = $this->solveSecurityIssues($client['log_session_data']);
      $client['log_content'] = $client['log_browser_info']['browser'].' / '.$client['log_browser_info']['platform'].' / '.$client['log_ip'];
      $r = (new ReflectionClass($client['log_class']))->getMethod($client['log_method'])->getDocComment();
      if (!$r)
        $r = 'No disponible';
      else {
        $r = str_replace('/** ', '', $r);
        $r = str_replace(' */', '', $r);
      }
      $client['log_special'] = $r;
      
      array_push($clients, $client);
    }
    return $clients;
  }

  public function solveSecurityIssues($v, $key = ''){
    if (is_array($v)){
			foreach($v as $key => $value)
				$v[$key] = $this->solveSecurityIssues($v[$key], $key);
		} else {
      $sensitive = array('password', 'token');
      $flag = false;
      for($i = 0; $i < count($sensitive); $i++){
        if (strpos($key, $sensitive[$i]) !== false){
          $flag = true;
        }
      }
      if ($flag)
        $v = '******';
		}
    return $v;
  }

  public function getBrowser(){
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version = "";
    if (preg_match('/linux/i', $u_agent)) {
      $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
      $platform = 'mac';
    } elseif (preg_match('/windows|win32/i', $u_agent)) {
      $platform = 'windows';
    }
    if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
      $bname = 'Internet Explorer';
      $ub = "MSIE";
    } elseif (preg_match('/Firefox/i', $u_agent)) {
      $bname = 'Mozilla Firefox';
      $ub = "Firefox";
    } elseif (preg_match('/Chrome/i', $u_agent)) {
      $bname = 'Google Chrome';
      $ub = "Chrome";
    } elseif (preg_match('/Safari/i', $u_agent)) {
      $bname = 'Apple Safari';
      $ub = "Safari";
    } elseif (preg_match('/Opera/i', $u_agent)) {
      $bname = 'Opera';
      $ub = "Opera";
    } elseif (preg_match('/Netscape/i', $u_agent)) {
      $bname = 'Netscape';
      $ub = "Netscape";
    } else {
      $bname = 'Unknown';
      $ub = "Unknown";
    }
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
      ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
    }
    $i = count($matches['browser']);
    if ($i != 1) {
      if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
        $version = @$matches['version'][0];
      } else {
        $version = @$matches['version'][1];
      }
    } else {
      $version = $matches['version'][0];
    }
    if ($version == null || $version == "") {
      $version = "?";
    }
    return array(
      'browser'      => $bname,
      'version'   => $version,
      'platform'  => $platform,
    );
  }

  public function create($class, $method, $obs = ''){
    $id = '';
    if ($class == 'logs' && $method == 'getLogs'){
      return Gral::response('true', $id);
    }
    if ($class == 'logs' && $method == 'loadResources'){
      return Gral::response('true', $id);
    }
    if ($class == 'logs' && $method == 'getLogsByModule'){
      return Gral::response('true', $id);
    }
    if ($class == 'login' && $method == 'logged' && !isset($_SESSION['use_id'])){
      return Gral::response('true', $id);
    }
    try {
      $new = $_SESSION;
      unset($new['logged']);
      unset($new['usu_id']);
      unset($new['same_password']);
      unset($new['per_firstname']);
      unset($new['per_lastname']);
      unset($new['per_surname']);
      $post = $_POST;
      unset($post['class']);
      unset($post['method']);
      $post = $this->solveSecurityIssues($post);
      $data = array(
        'log_use_id' => isset($_SESSION['use_id']) ? $_SESSION['use_id'] : 'invitado',
        'log_class' => $class,
        'log_method' => $method,
        'log_post_data' => json_encode($post),
        'log_session_data' => json_encode($new),
        'log_browser_info' => json_encode($this->getBrowser()),
        'log_ip' => $_SERVER['REMOTE_ADDR'],
        'log_obs' => $obs,
      );
      $id = $this->insert("NULL", $data);
    } catch (MarssoftError $e) {
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  /* SQL RULES */

  public function sql_rules(){
    $this->create_table();
    if (!$this->is_ok($this->foreign_keys, $this->rows)) {
      $this->create_fields($this->foreign_keys, 0, true);
      $this->create_fields($this->rows, 0);
    }
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array(
    array('use_id', 'varchar(30)', 'NOT NULL')
  );
  /**
   * Set row keys
   */
  public $rows = array(
    array('class', 'varchar(100)', 'NOT NULL'),
    array('method', 'varchar(100)', 'NOT NULL'),
    array('post_data', 'TEXT', 'NOT NULL'),
    array('session_data', 'TEXT', 'NOT NULL'),
    array('browser_info', 'TEXT', 'NOT NULL'),
    array('ip', 'varchar(20)', 'NOT NULL'),
    array('proxy', 'varchar(20)', 'NOT NULL'),
    array('obs', 'TEXT', 'NOT NULL'),

    array('deleted', 'int(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}
