<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Config extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
    $this->sql_rules();
  }

  public function single($id){
    Ws::$c->q("SELECT * FROM config WHERE con_key = '$id' LIMIT 1;");
    if (Ws::$c->nr() > 0){
      $client = Ws::$c->fa();
    } else {
      $client = array();
    }
    return $client;
  }

  public function performUpdate(){
    $value = $this->single('version');
    $update = true;
    if (count($value) == 0){
      $this->create(array(
        'con_key' => 'version',
        'con_value' => Version::$version,
        'con_descr' => 'Last version: '.date('Y-m-d H:i:s'),
      ));
      $update = false;
    } else {
      if ($value['con_value'] == Version::$version){
        $update = false;
      } else {
        $this->update(array(
          'con_id' => $value['con_id'],
          'con_key' => 'version',
          'con_value' => Version::$version,
          'con_descr' => 'Last version: '.date('Y-m-d H:i:s'),
        ));
      }
    }
    return true; //$update;
  }

  public function update($data){
    Ws::$c->q("UPDATE config SET con_key = '$data[con_key]', con_value = '$data[con_value]', con_descr = '$data[con_descr]' WHERE con_id = $data[con_id] LIMIT 1;");
    return array('response' => 'true');
  }
  public function create($data){
    Ws::$c->q("INSERT INTO config VALUES(NULL, '$data[con_key]', '$data[con_value]', '$data[con_descr]')");
    return array('response' => 'true');
  }

  public function sql_rules(){
    $this->create_table();
    if (!$this->is_ok($this->foreign_keys, $this->rows)){
      $this->create_fields($this->foreign_keys, 0, true);
      $this->create_fields($this->rows, 0);
    }
  }

  public $foreign_keys = array(
      
  );
  public $rows = array(
    array('key', 'varchar(100)', 'NOT NULL'),
    array('value', 'varchar(100)', 'NOT NULL'),
    array('descr', 'varchar(100)', 'NOT NULL'),

  );
}

?>