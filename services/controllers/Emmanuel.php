<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Emmanuel extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }

  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        $id = isset($_SESSION["emm_id"]) ? $_SESSION["emm_id"] : $_POST['id'];
        return $this->update($_POST['data'], $id);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
    }
  }

  /** Eliminación de un emmanuel */

  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  /** Acceso a un emmanuel */

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $single = Ws::$c->fa();
    $_SESSION['emm_id'] = $single['emm_id'];
    $single = $this->xss_client($single);
    $single['emm_date_of_birth'] = $this->toDate($single['emm_date_of_birth']);
    return $single;
  }

  /** Acceso a la lista de emmanuel */

  public function get(){
    $this->sget(Ws::$c);
    $array = array();
    while($row = Ws::$c->fa()){
      $client = $this->xss_client($row);
      array_push($array, $client);
    }
    return $array;
  }

  /** Actualización de un emmanuel */

  public function update($data, $id){
    try {
      if (isset($data['emm_date_of_birth']))
        $data['emm_date_of_birth'] = date('Y-m-d', strtotime(Ws::$g->stringDateToNumber($data['emm_date_of_birth'])));
      $this->upd($id, $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  /** Creación de un emmanuel */

  public function create($data){
    try {
      $data['emm_date_of_birth'] = date('Y-m-d', strtotime(Ws::$g->stringDateToNumber($data['emm_date_of_birth'])));
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function sql_rules(){
    $this->create_table(0);
    if (!$this->is_ok($this->foreign_keys, $this->rows)){
      $this->create_fields($this->foreign_keys, 0, true);
      $this->create_fields($this->rows, 0);
    }
  }

  public $foreign_keys = array(
      
  );
  
  public $rows = array(
    array('name', 'varchar(100)', 'NOT NULL'),
    array('age', 'int(3)', 'NOT NULL'),
    array('date_of_birth', 'date', 'NOT NULL'),

    array('deleted', 'int(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

/*
 La herramienta generará lo siguiente:

CREATE TABLE emmanuel (
  fer_id int(6) AUTO_INCREMENT NOT NULL,
  fer_name varchar (100) NO NULL
  fer_age int(3) NOT NULL,
  fer_date_of_birth date NOT NULL,
  fer_deleted int(1) NOT NULL, 
  fer_created_at datetime NOT NULL,
  fer_update_at datetime NOT NULL,
) */

?>