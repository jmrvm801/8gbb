<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class juan extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }

  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        $id = isset($_SESSION["jua_id"]) ? $_SESSION["jua_id"] : $_POST['id'];
        return $this->update($_POST['data'], $id);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
    }
  }

  /** Eliminación de un juan */

  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  /** Acceso a un juan */

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $single = Ws::$c->fa();
    $_SESSION['jua_id'] = $single['jua_id'];
    $single = $this->xss_client($single);
    $single['jua_date_of_brirth'] = $this->toDate($single['jua_date_of_brirth']);
    return $single;
  }

  /** Acceso a la lista de juan */

  public function get(){
    $this->sget(Ws::$c);
    $array = array();
    while($row = Ws::$c->fa()){
      $client = $this->xss_client($row);
      array_push($array, $client);
    }
    return $array;
  }

  /** Actualización de un juan */

  public function update($data, $id){
    try {
      if (isset($data['jua_date_of_brirth']))
        $data['jua_date_of_brirth'] = date('Y-m-d', strtotime(Ws::$g->stringDateToNumber($data['jua_date_of_brirth'])));
      $this->upd($id, $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  /** Creación de un juan */

  public function create($data){
    try {
      $data['jua_date_of_brirth'] = date('Y-m-d', strtotime(Ws::$g->stringDateToNumber($data['jua_date_of_brirth'])));
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function sql_rules(){
    $this->create_table(0);
    if (!$this->is_ok($this->foreign_keys, $this->rows)){
      $this->create_fields($this->foreign_keys, 0, true);
      $this->create_fields($this->rows, 0);
    }
  }

  public $foreign_keys = array(
      
  );
  
  public $rows = array(
    array('name', 'varchar(100)', 'NOT NULL'),
    array('age', 'int(3)', 'NOT NULL'),
    array('date_of_brirth', 'date', 'NOT NULL'),

    array('deleted', 'int(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>