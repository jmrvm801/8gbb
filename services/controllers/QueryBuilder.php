<?php
class QueryBuilder extends user{
  private $class = '';
  private $short = '';
  public function __construct($class){
    $this->class = strtolower(get_class($class));
    $this->short = substr($this->class, 0, 3);
  }

  public function hasAccess($return = false){
    if (!isset($_SESSION['use_profile'])){
      $this->terminate();
    }
    $numargs = func_num_args();
    $arg_list = func_get_args();
    if ($numargs == 1){
      if ($_SESSION['use_profile'] > $arg_list[0]){
        $this->terminate();
      }
    } else if ($numargs > 1){
      if (array_search($_SESSION['use_profile'], $arg_list) === false){
        $this->terminate();
      }
    }
    if ($return)
      return true;
  }

  /**
   * Finishes the program
   */

  public function terminate($code = 'No se puede garantizar la autenticidad de la petición'){
      $array = array(
        'response_error' => 'access_denied',
        'code' => $code,
      );
      die(json_encode($array));
  }

  /**
   * Save a file into a specified route.
   * 
   * @param Array $data who contents file base64-based
   * @param Mixed $id of file.
   * @param String $place to allocate the file [optional][default = payments]
   */

  public function setFile($data, $id, $place = 'payments'){
    //If the file are an image, compress it.
    $data['document'] = explode(';base64,', $data['document']);
    $data['document'] = $data['document'][1];
    $content = base64_decode($data['document']);
    $name = '../resources/'.$place.'/'.$id.'.'.strtolower($data['doc_type']);
    $file = fopen($name, "wb");
    fwrite($file, $content);
    fclose($file);
    return $name;
  }

  public function toDate($date){
    $time = strtotime($date);
    if ($date == '1969-12-31')
      return array(
        'string' => 'Sin fecha',
        'date' => $date,
        'current' => date('Ymd', $time),
        'unix' => $time,
        'js' => date('D M d Y H:i:s O', $time),
      );
    return array(
      'string' => Ws::$g->numberDateToString($date),
      'date' => $date,
      'current' => date('Ymd', $time),
      'unix' => $time,
      'js' => date('D M d Y H:i:s O', $time),
    );
  }

  /**
   * Converts String date into a specified format
   * 
   * @param String Date $date to convert
   * @param String $format [optional][default = Y-m-d]
   * 
   * @return current format based on $format param
   */

  public function toUTC($date, $format = 'Y-m-d'){
    return date($format, strtotime(Ws::$g->stringDateToNumber($date)));
  }

  /**
   * Verifies that rows to insert or update exists on database based on row class
   * 
   * @param Array $rows to check
   * 
   * @return Array true or false
   */

  public function rowPolicies($rows){
    $not_found = array();
    $tmps = array();
    $str = array($this->foreign_keys, $this->rows);
    for($j = 0; $j < count($str); $j++){
      for($i = 0; $i < count($str[$j]); $i++){
        $tmp = $this->short."_".$str[$j][$i][0];
        array_push($tmps, $tmp);
      }
    }
    foreach ($rows as $key => $value){
      if (array_search($key, $tmps) === false)
        array_push($not_found, $key);
    }
    return array(
      'response' => count($not_found) > 0 ? 'incomplete' : 'complete',
      'data' => $not_found
    );
  }

  /**
   * Convert strings comming from client-side
   * 
   * @param Array-Matrix $array to convert each data
   * @param Boolean $recursive to codify all inner array. [optional][default = true]
   * 
   * @return Array-Matrix codified
   */

  public function utf8_server($array, $recursive = true){
    // foreach ($array as $key => $value)
    //   $array[$key] = (is_array($value) && $recursive) ? $this->utf8_server($value, $recursive) : utf8_decode($value);
    return $array;
  }
  
  /**
   * Convert strings comming from server-side
   * 
   * @param Array-Matrix $array to convert each data
   * @param Boolean $recursive to codify all inner array. [optional][default = true]
   * 
   * @return Array-Matrix codified
   */

	public function xss_client($array, $recursive = true){
    foreach ($array as $key => $value)
      $array[$key] = (is_array($value) && $recursive) ? $this->xss_client($value, $recursive) : htmlspecialchars_decode($value, ENT_QUOTES);
    return $array;
  }

  /**
   * Insert data into database
   * 
   * @param $primary
   * @param Array $items to insert
   * @param Restrict $restrict information to insert into database[optional][default = array()]
   * 
   * @return last id inserted
   */
  
  public function insert($primary, $items, $restrict = array()){
    $result = $this->rowPolicies($items);
    if ($result['response'] == 'incomplete')
      throw new MarssoftError('Some rows doesnt exists on database', $result, 1000);
    $tags = array();
    $fields = array();
    if ($primary != "NULL"){
      array_push($tags, "".$this->short."_id");
      array_push($fields, "'$primary'");
    }
    foreach ($items as $key => $value){
      if (array_search($key, $restrict) === false){
        array_push($tags, $key);
        array_push($fields, (($value != "NULL") ? "'$value'" : $value));
      }
    }
    if (count($fields) == 0)
      return false;
    array_push($tags, "".$this->short."_deleted");
    array_push($fields, "'0'");
    array_push($tags, "".$this->short."_created_at");
    array_push($fields, "'".date('Y-m-d H:i:s')."'");
    array_push($tags, "".$this->short."_updated_at");
    array_push($fields, "'".date('Y-m-d H:i:s')."'");
    $fields = implode(", ", $fields);
    $tags = implode(", ", $tags);
    $query = "INSERT INTO $this->class($tags) VALUES($fields)";
    Ws::$c->q($query);
    return Ws::$c->last($this->class);
  }

  /**
   * update data into database
   * 
   * @param ID $id to update single record
   * @param Array $items to update
   * @param Where $custom_where to set condition to update [optional][detault = ""]
   * @param Restrict $restrict information to update based on $items [optional][default = array()]
   * 
   * @return bool true
   */

  public function upd($id, $items, $custom_where = "", $restrict = array()){
    $result = $this->rowPolicies($items);
    if ($result['response'] == 'incomplete')
      throw new MarssoftError('Some rows doesnt exists on database', $result, 1001);
    $custom_where = ($custom_where == "") ? "$this->short"."_id = '$id' LIMIT 1;" : $custom_where;
    $fields = array();
    foreach ($items as $key => $value)
      if (array_search($key, $restrict) === false)
        array_push($fields, "".$key." = ".(($value != "NULL") ? "'$value'" : $value));
    if (count($fields) == 0)
      return false;
    $fields = implode(", ", $fields);
    $query = "UPDATE $this->class SET $fields, $this->short"."_updated_at = '".date('Y-m-d H:i:s')."' WHERE $custom_where";
    Ws::$c->q($query);
    return true;
  }

  /**
   * get all information from a table
   * 
   * @param Connection $con to database
   * @param Where clause $where filter data [optional][default = ""]
   * @param Order clause $order order to show information [optional][default = ""]
   * 
   * @return void
   */

  public function sget($con, $where = "", $order = ""){
    $where = ($where == "") ? $where : $where." AND";
    $order = ($order == "") ? "$this->short"."_id DESC" : $order;
    $query = "SELECT * FROM $this->class WHERE $where $this->short"."_deleted = '0' ORDER BY $order;";
    $con->q($query);
  }

  /**
   * Get a single information from a table based on a id
   * 
   * @param Connection $con to database
   * @param Id $id from the table
   * @param Where $where condition [optional][default = ""]
   */

  public function bsingle($con, $id, $where = ""){
    $where = ($where == "") ? "$this->short"."_id = '$id'" : $where;
    $con->q("SELECT * FROM $this->class WHERE $where LIMIT 1;");
  }

  /**
   * Delete logically information from a table based on a id
   * 
   * @param Id $id from the table
   * @param Where $where condition [optional][default = ""]
   */
  
  public function remove($id, $where = ""){
    $where = ($where == "") ? "$this->short"."_id = '$id'" : $where;
    Ws::$c->q("UPDATE $this->class SET $this->short"."_deleted = '1', $this->short"."_updated_at = '".date('Y-m-d H:i:s')."' WHERE $where LIMIT 1;");
  }

  /**
   * Delete logically information from a table based on a id
   * 
   * @param Id $id from the table
   * @param Where $where condition [optional][default = ""]
   */
  
  public function removePhysical($id, $where = ""){
    $where = ($where == "") ? "$this->short"."_id = '$id'" : $where;
    Ws::$c->q("DELETE FROM $this->class WHERE $where LIMIT 1;");
  }

  public function utf8encode($array, $properties){
    for($i = 0; $i < count($properties); $i++)
      $array[$properties[$i]] = utf8_encode($array[$properties[$i]]);
    return $array;
  }

  public function is_ok($foreign, $rows){
    $total = 1 + count($foreign) + count($rows);
    Ws::$c->q("SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = '$this->baseDeDatos' AND table_name = '$this->class'");
    return (Ws::$c->r() == $total);
  }

  public function perm($level){
    return ($_SESSION['use_profile'] < $level);
  }

  public function create_table($attemps = 0, $type = "int(6) AUTO_INCREMENT"){
    Ws::$c->q("SHOW TABLES LIKE '$this->class';");
    if (Ws::$c->nr() == 0){
      $sql = "CREATE TABLE $this->class (".$this->short."_id $type, PRIMARY KEY (".$this->short."_id) );";
      Ws::$c->q($sql);
      if ($attemps < 3)
        $this->create_table($attemps + 1);
      else
        die(json_encode(array('response_error' => 'Cannot create table '.$this->class)));
    }
  }
  
  public function create_fields($fields, $i = 0, $foreign = false, $attemps = 0){
    if ($i <= count($fields) -1){
      $after = ($i > 0) ? " AFTER ".$this->short."_".$fields[$i-1][0]." " : "";
      if ($i == 0 && $foreign)
        $after = "AFTER ".$this->short."_id";
      Ws::$c->q("SHOW COLUMNS FROM `$this->class` LIKE '".$this->short."_".$fields[$i][0]."';");
      if (Ws::$c->nr() == 0){
        $sql = "ALTER TABLE $this->class ADD COLUMN ".$this->short."_".$fields[$i][0]." ".$fields[$i][1]." ".$fields[$i][2]." ".$after;
        Ws::$c->q($sql);
        if ($attemps < 3)
          $this->create_fields($fields, $i, $foreign, $attemps + 1);
        else
          die(json_encode(array('response_error' => 'Cannot create column '.$fields[$i][0])));
      } else {
        $this->create_fields($fields, $i + 1, $foreign, 0);
      }  
    }
  }

  public function sendcorreo($titulo, $content, $mail){
		$contenido = '<table width="800" align="center" border="0" style="margin:auto;font-family:sans-serif;background:#FFF;">
    	<tr style="border-bottom:none;">
        <tr style="border-bottom:none;">
        	<td style="font-size:24px;text-align:center;">'.$titulo.'</td>
        </tr>
        <tr style="border-bottom:none;">
        	<td>
            	<table width="740" align="center" border="0" style="margin:20px auto;background:#F0F0F0;">
                    <tr style="border-bottom:none;">
                        <td style="padding:15px;font-size:17px;">'.$content.'</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
    $contenido = '
      <style> @font-face { font-family: \'Roboto\'; font-style: normal; font-weight: 400; src: local(\'Roboto\'), local(\'Roboto-Regular\'), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxK.woff2) format(\'woff2\'); unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;} </style>
        <table class="table" style="color: #444;font-family: \'Roboto\';width: 1000px;line-height: 1.5em;" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="font-size: 28px;text-align: left;padding:5px 100px;">Instituto Anglo A.C.</td>
                <td style="padding:5px 100px;text-align: right;"><img src="https://sistemas.institutoanglo.edu.mx/assets/img/logo_abc.png" alt="Armesis" width="80" /></td>
            </tr>
            <tr>
                <td colspan="2" style="height: 40px;"></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;font-size: 30px;padding:50px;">'.$titulo.'</td>
            </tr>
        </table>
        <table class="table" style="font-family: \'Roboto\';width: 1000px;line-height: 1.5em;" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="padding: 30px 100px 130px;line-height: 1.5em;font-size: 18px;text-align: justify;">
                    '.$content.'
                </td>
            </tr>
        </table>
        <table class="table" style="font-family: \'Roboto\';width: 1000px;line-height: 1.5em;" border="0" cellspacing="0" cellpadding="0">
            <tr style="background: #F0F0F0">
                <td style="padding: 45px 100px 15px;line-height: 1.5em;font-size: 20px;color:#444;">
                Instituto Anglo A.C.
                </td>
            </tr>
            <tr style="background: #F0F0F0">
                <td style="padding: 0px 100px 30px;line-height: 1.5em;font-size: 16px;">
                    Desarrollado en Grupo Marssoft
                </td>
            </tr>
        </table>
    ';
		$headers = "MIME-Version: 1.0\r\n"; 
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
		$headers .= 'From: Instituto Anglo A.C. <noreply@sistemas.institutoanglo.edu.mx>\r\n'; 
		return mail($mail, utf8_decode($titulo), utf8_decode($contenido), $headers);
	}
}
?>