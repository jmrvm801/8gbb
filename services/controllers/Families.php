<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Families extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }

  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['data']);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
    }
  }

  /** Eliminación de una familia */

  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  /** Acceso a una familia única */

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $_SESSION['fam_id'] = $id;
    $single = Ws::$c->fa();
    $single = $this->xss_client($single);
    return $single;
  }

  /** Acceso a la lista de familias */

  public function get(){
    $d = new db();
    $this->sget($d, "", "fam_name");
    $array = array();
    $sub = new Subfamilies();
    while($row = $d->fa()){
      $row = $this->xss_client($row);
      $row['subfamilies_total'] = count($sub->getSubfamiliesByFamId($row['fam_id']));
      array_push($array, $row);
    }
    $d->cl();
    return $array;
  }

  /** Actualización de una familia */

  public function update($data){
    $data = $this->utf8_server($data);
    try {
      $this->upd($_SESSION["fam_id"], $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  /** Creación de una familia */

  public function create($data){
    $data = $this->utf8_server($data);
    try {
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function sql_rules(){
    $this->create_table();
    if (!$this->is_ok($this->foreign_keys, $this->rows)){
      $this->create_fields($this->foreign_keys, 0, true);
      $this->create_fields($this->rows, 0);
    }
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array();
  /**
   * Set row keys
   */
  public $rows = array(
    array('name', 'varchar(100)', 'NOT NULL'),
    array('deleted', 'varchar(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>