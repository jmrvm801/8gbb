<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Martin extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }

  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        $id = isset($_SESSION["mar_id"]) ? $_SESSION["mar_id"] : $_POST['id'];
        return $this->update($_POST['data'], $id);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
    }
  }

  /** Eliminación de un Martin */

  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  /** Acceso a un Martin */

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $single = Ws::$c->fa();
    $_SESSION['mar_id'] = $single['mar_id'];
    $single = $this->xss_client($single);
    $single['mar_date_of_birth'] = $this->toDate($single['mar_date_of_birth']);
    return $single;
  }

  /** Acceso a la lista de Martin */

  public function get(){
    $this->sget(Ws::$c);
    $array = array();
    while($row = Ws::$c->fa()){
      $client = $this->xss_client($row);
      array_push($array, $client);
    }
    return $array;
  }

  /** Actualización de un Martin */

  public function update($data, $id){
    try {
      if (isset($data['mar_date_of_birth']))
        $data['mar_date_of_birth'] = date('Y-m-d', strtotime(Ws::$g->stringDateToNumber($data['mar_date_of_birth'])));
      $this->upd($id, $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  /** Creación de un Martin */

  public function create($data){
    try {
      $data['mar_date_of_birth'] = date('Y-m-d', strtotime(Ws::$g->stringDateToNumber($data['mar_date_of_birth'])));
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function sql_rules(){
    $this->create_table(0);
    if (!$this->is_ok($this->foreign_keys, $this->rows)){
      $this->create_fields($this->foreign_keys, 0, true);
      $this->create_fields($this->rows, 0);
    }
  }

  public $foreign_keys = array(
      
  );
  
  public $rows = array(
    array('name', 'varchar(100)', 'NOT NULL'),
    array('age', 'int(3)', 'NOT NULL'),
    array('date_of_birth', 'date', 'NOT NULL'),

    array('deleted', 'int(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>