<?php
class Login
{
  public function __construct(){
  }

  public function run($method = 'default')
  {
    switch ($method) {
      case 'grant':
        return $this->{$method}($_POST['data']);
      case 'forgotPassword':
        return $this->{$method}($_POST['data']);
      case 'setMailInfo':
        return $this->{$method}($_POST['data']);
      case 'validateCode':
        return $this->{$method}($_POST['data']);
      case 'changePassword':
        return $this->{$method}($_POST['data']);
      case 'logout':
        return $this->{$method}();
      case 'logged':
        return $this->{$method}();
    }
  }

  /** Cambio de contraseña */

  public function changePassword($data){
    if ($_SESSION['validateCode'] == '0'){
      return array('response' => 'error');
    }
    (new Users())->changePassword($data['per_password'], $_SESSION['tmp_username']);
    return array('response' => 'true');
  }

  /** Validación de código */

  public function validateCode($data){
    if ($_SESSION['code'] == $data['per_code_use']){
      $_SESSION['validateCode'] = '1';
      return array('response' => 'true');
    } else {
      $_SESSION['validateCode'] = '0';
      return array('response' => 'error');
    }
  }

  /** Envío de correo electrónico */

  public function setMailInfo($data){
    $user = new Users();
    $_SESSION['use_profile'] = '11';
    $_SESSION['use_id'] = $_SESSION['tmp_username'];
    $res = $user->single($_SESSION['use_id']);
    if ($res['use_profile'] == '1'){
      return array('response' => 'cannot_restore');
    }
    $mail = array(
      $res['personal']['per_email']
    );
    if ($mail[$data['per_email']] != $data['per_confirm_email']){
      return array('response' => 'mailError');
    } else {
      $qb = new QueryBuilder($this);
      $code = rand(100000, 999999);
      $_SESSION['code'] = $code;
      @$qb->sendcorreo('Código de seguridad Instituto Anglo', 'Hola. Le apoyaremos a recuperar su cuenta del instituto Anglo. Para recuperar su cuenta ingrese el siguiente código:<br><br>'.$code.'<br><br>Favor de no responder este correo puesto que es automatizado.', $data['per_confirm_email']);
      return array('response' => 'true', 'code' => $code); //
    }
  }

  /** Acceso a procedimientos de recuperación de cuenta */

  public function forgotPassword($data){
    $user = new Users();
    $_SESSION['use_profile'] = '11';
    $_SESSION['use_id'] = $data['username'];
    $_SESSION['tmp_username'] = $data['username'];
    $res = $user->single($data['username']);
    $res['use_locked_at'] = ($res['use_locked_at'] == '') ? date('Y-m-d H:i:s') : $res['use_locked_at'];
    if (strtotime(date('Y-m-d H:i:s')) < strtotime($res['use_locked_at']))
      return array('response' => 'locked', 'time_at' => $res['use_locked_at']);
    if ($res['use_profile'] == '1'){
      return array('response' => 'cannot_restore');
    }
    $mail = array(
      $res['personal']['per_email']
    );
    $clean = 0;
    for($i = 0; $i < count($mail); $i++){
      if ($mail[$i] == ''){
        $clean++;
      } else {
        $mail[$i] = explode('@', $mail[$i]);
        $mail[$i][0] = str_split($mail[$i][0]);
        for($j = 3; $j < count($mail[$i][0]); $j++){
          $mail[$i][0][$j] = '*';
        }
        $mail[$i][0] = implode("", $mail[$i][0]);
        $mail[$i][1] = str_split($mail[$i][1]);
        for($j = 3; $j < count($mail[$i][1]); $j++){
          $mail[$i][1][$j] = '*';
        }
        $mail[$i][1] = implode("", $mail[$i][1]);
        $mail[$i] = array(
          'sch_id' => $i,
          'sch_mail' => $mail[$i][0] .'@'. $mail[$i][1],
        );
      }
    }
    if ($clean == count($mail)){
      return array('response' => 'hasnt_mail');
    }
    return array(
      'response' => 'true',
      'data' => $mail
    );
  }

  /** Cierre de sesión */

  public function logout(){
    session_destroy();
    $array = array();
    foreach($_COOKIE as $key => $value){
      Ws::$g->cook($key, '', 1);
      array_push($array, $key);
    }
    return array('response' => 'true', 'cookies_deleted' => $array);
  }

  /** Inicio de sesión */
  
  public function grant($datam, $super = false){
    $logs = new Logs();
    Ws::$c->q("SELECT use_id, use_password, use_locked_at, per_gender, use_attemps, per_firstname, per_lastname, per_surname, use_profile, use_logs, use_current_login, use_last_login, use_token FROM users LEFT JOIN personals ON use_per_id = per_id WHERE use_id = '$datam[username]' AND use_deleted = '0' LIMIT 1;");
    if (Ws::$c->nr() == 1) {
      $data = Ws::$c->fa();
      $data['use_locked_at'] = ($data['use_locked_at'] == '') ? date('Y-m-d H:i:s') : $data['use_locked_at'];
      if (strtotime(date('Y-m-d H:i:s')) < strtotime($data['use_locked_at']))
        return array('response' => 'locked', 'time_at' => $data['use_locked_at']);
      if ($datam['password'] == $data['use_id']){
        $_SESSION['same_password'] = 'true';
        Ws::$g->cook('same_password', '1', 7200);
      } else {
        $_SESSION['same_password'] = 'false';
        Ws::$g->cook('same_password', '0', 7200);
      }
      if (!password_verify($datam['password'], $data['use_password']) && !$super){
        (new Logs())->create($_POST['class'], $_POST['method'], 'Acceso denegado para el usuario '.$datam['username']);
        $data['use_attemps'] += 1;
        Ws::$c->q("UPDATE users SET use_attemps = '$data[use_attemps]' WHERE use_id = '$datam[username]' LIMIT 1;");
        if ($data['use_attemps'] == 3){
          (new Logs())->create($_POST['class'], $_POST['method'], 'Acceso bloqueado para el usuario '.$datam['username']);
          $date = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' + 15 minutes'));
          Ws::$c->q("UPDATE users SET use_locked_at = '$date' WHERE use_id = '$datam[username]' LIMIT 1;");
          return array('response' => 'locked', 'time_at' => $date);
        } else
          return array('response' => 'denied');
      }
      $data['use_last_login'] = $data['use_current_login'];
      $data['use_current_login'] = date('Y-m-d H:i:s');
      $_SESSION['current_login'] = $data['use_current_login'];
      $data['use_logs']++;
      $token = bin2hex(random_bytes(8));
      if ($data['use_token'] == ''){
        $data['use_token'] = array();
      } else {
        $data['use_token'] = json_decode($data['use_token']);
      }
      array_push($data['use_token'], $token);
      $data['use_token'] = json_encode($data['use_token']);
      Ws::$c->q("UPDATE users SET use_token = '$data[use_token]', use_attemps = '0', use_last_login = '$data[use_last_login]', use_current_login = '$data[use_current_login]', use_logs = '$data[use_logs]', use_locked_at = NULL WHERE use_id = '$datam[username]' LIMIT 1;");
      $array = array('use_id', 'per_firstname', 'per_lastname', 'per_surname', 'use_profile', 'per_gender');
      $data['per_firstname'] = base64_encode(utf8_decode($data['per_firstname']));
      $data['per_lastname'] = base64_encode(utf8_decode($data['per_lastname']));
      $data['per_surname'] = base64_encode(utf8_decode($data['per_surname']));
      for ($i = 0; $i < count($array); $i++) {
        Ws::$g->cook($array[$i], is_array($data[$array[$i]]) ? json_encode($data[$array[$i]]) : $data[$array[$i]], 7200);
        $_SESSION[$array[$i]] = $data[$array[$i]];
      }
      $_SESSION['logged'] = 1;
      $single = (new Users())->single($datam['username']);
      return array(
        'response' => 'true',
        'user_data' => $single,
        'token' => $token
      );
    } else
      return array('response' => 'denied');
  }

  /** Recarga de la página web */

  public function logged(){
    $response = false;
    if (isset($_SESSION['logged']))
      $response = ($_SESSION['logged'] == 1);
    return array(
      'response' => $response,
      'user_data' => isset($_SESSION['use_id']) ? (new Users())->single($_SESSION['use_id']) : array()
    );
  }
}
