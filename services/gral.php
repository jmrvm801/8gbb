<?php
class Gral{
	public $abrv = array('Ene' => '01', 'Feb' => '02', 'Mar' => '03', 'Abr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Ago' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dic' => '12');
	public $nmx = array('01' => 'Ene', '02' => 'Feb', '03' => 'Mar', '04' => 'Abr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Ago', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dic');
	function __construct(){
		session_start();
		if ($this->activateXssProtection()){
			foreach ($_POST as $clave => $valor)
				$_POST[$clave] = $this->xss($_POST[$clave]);
		}
	}

	public function activateXssProtection(){
		if (isset($_POST['class']) && isset($_POST['method'])){
			if ($_POST['class'] == 'contents' && $_POST['method'] == 'create')
				return false;
			if ($_POST['class'] == 'contents' && $_POST['method'] == 'update')
				return false;
			return true;
		} else
			return true;
	}

	public static function response($status, $id = "", $data = ""){
		return array(
			'response' => $status,
			'id' => $id,
			'data' => $data
		);
	}

	public static function error($status, $data = "", $code = ""){
		return array(
			'response' => $status,
			'response_error' => $status,
			'data' => $data,
			'code' => $code,
		);
	}

	public function stringDateToNumber($str){
		if ($str == ' ' || $str == '')
				return $str;
		$str = explode(' ', $str);
		$str[1] = str_replace(',','',$str[1]);
		$str[0] = $this->abrv[$str[0]];
		return $str[2].''.$str[0].''.$str[1];
	}

	public function br2nl($str){
		$breaks = array("<br />","<br>","<br/>");  
    return str_ireplace($breaks, "\r\n", $str);
	}

	public function numberDateToString($str){
		if ($str == ' ' || $str == '')
				return $str;
		$str = str_split($str);
		$year = $str[0].''.$str[1].''.$str[2].''.$str[3];
		$month = $this->nmx[$str[5].''.$str[6]];
		$day = $str[8].''.$str[9];
		return $month.' '.$day.', '.$year;
	}
	
	function cook($name,$value,$future=10800){
		setcookie($name, $value, time()+$future, "/", "", 0);
	}

	function normaliza ($cadena){
		$originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
		$modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
		$cadena = utf8_decode($cadena);
		$cadena = strtr($cadena, utf8_decode($originales), $modificadas);
		$cadena = strtolower($cadena);
		return utf8_encode($cadena);
	}

	function json($v,$data){
		return ($v) ? json_encode($data) : json_decode($data);
	}

	function srfecha($dia, $ret, $fecha = ''){
		$fecha = ($fecha == '') ? date('YmdHis') : $fecha;
		$fecha = strtotime($dia.' day', strtotime($fecha));
		return date($ret, $fecha);
	}

	function jsonO($v,$data){
		return ($v) ? json_encode($data, JSON_FORCE_OBJECT) : json_decode($data);
	}

	function utf8($v,$data){
		if (!is_array($data))
			$data = ($v) ? utf8_encode($data) : utf8_decode($data);
		else
			foreach ($data as $i => $v)
				$data[$i] = ($v) ? utf8_encode($data[$i]) : utf8_decode($data[$i]);
		return $data;
	}

	function kill($msn){
		die($msn);
	}

	function xss($v){
		$_e = array("'", "<",">","&");
		$_r = array("#039;", "&lt;", "&gt;","&amp;");
		if (is_array($v)){
			foreach($v as $key => $value)
				$v[$key] = $this->xss($v[$key]);
		} else {
			$v = htmlspecialchars($v, ENT_QUOTES);
			$v = trim($v);
			// $v = str_replace($_e, $_r, $v);
		}
		return $v;
	}

	function jsonx($v){
		$v = $this->xss($v);
		$v = $this->json(false, $v);
		return $this->xss($v);
	}

	function isn($v){
		if (!is_array($v))
			return is_numeric($v);
		else {
			$vs = true;
			for ($i = 0; $i < count($v); $i++)
				if (!is_numeric($v[$i]))
					$vs = false;
			return $vs;
		}
	}
}