<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="theme-color" content="#e9ab56">
		<title>Cargando sistema...</title>
		<link rel="icon" href="assets/img/favicon.ico?">
		<!-- Libs scripts -->
		<link href="assets/css/roboto/roboto.css" rel="stylesheet">
		<script type="text/javascript" src="https://marssoftcdn.b-cdn.net/jquery-3.3.1.js"></script>
		<script type="text/javascript" src="https://marssoftcdn.b-cdn.net/jquery-ui.js"></script>
		<script type="text/javascript" src="https://marssoftcdn.b-cdn.net/blurhouse.js"></script> <!-- assets/libs -->
		<script type="text/javascript" src="assets/libs/cookie.js"></script>
		<script type="text/javascript" src="assets/libs/qrcode.js"></script>
		<script type="text/javascript" src="https://marssoftcdn.b-cdn.net/materialize.js"></script>
		<script type="text/javascript" src="assets/libs/jquery.PrintArea.js"></script>
		<script type="text/javascript" src="assets/libs/util.js?random=<?php echo filemtime('assets/libs/util.js'); ?>"></script>
		<script type="text/javascript" src="https://marssoftcdn.b-cdn.net/moment.min.js"></script>
		<script type="text/javascript" src="assets/libs/suneditor.js"></script>
		<link rel="stylesheet" type="text/css" href="assets/css/datatables.min.css"/>
		<!-- <script type="text/javascript" src="assets/libs/vfs_fonts.js"></script> -->
		<!-- <script type="text/javascript" src="assets/libs/vfs_fonts.js"></script> -->
		<script type="text/javascript" src="https://marssoftcdn.b-cdn.net/datatables.min.js"></script>
		<script type="text/javascript" src="https://marssoftcdn.b-cdn.net/excel.datatables.min.js"></script>
		<script type="text/javascript" src="assets/libs/html2canvas.min.js"></script>
		<!-- <script type="text/javascript" src="assets/js/pdf-reports.js"></script> -->
		<script type="text/javascript" src="https://marssoftcdn.b-cdn.net/ag-grid-enterprise.js"></script>
		<!-- Business scripts -->
		<script type="text/javascript" src="assets/js/index.js?random=<?php echo filemtime('assets/js/index.js'); ?>"></script>
		<!-- CSS scripts -->
		<link type="text/css" rel="stylesheet" href="assets/css/core.css?random=<?php echo filemtime('assets/css/core.css'); ?>">
		<link type="text/css" rel="stylesheet" href="assets/css/suneditor.css">
    <link type="text/css" rel="stylesheet" href="assets/css/icon.css"/>
		<link type="text/css" rel="stylesheet" href="assets/css/materialize.css" media="screen,projection,print"/>
		<link type="text/css" rel="stylesheet" href="assets/css/index.css?random=<?php echo filemtime('assets/css/index.css'); ?>">
		<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
		<script>
			let version = <?= filemtime('assets/js/index.js'); ?>;
		</script>
	</head>
	<body class="grey lighten-3">
		<div class="app"></div>
		<div class="loaders"></div>
		<div id="secundary_modal" class="modal modal-fixed-footer">
			<div class="modal-content">
				<h4 class="questiontitle_secundary">Modal Header</h4>
				<p class="questiondescr_secundary">A bunch of text</p>
			</div>
			<div class="modal-footer">
				<button class="waves-effect waves-light btn red modal-close">Cancelar</button>
				<button class="waves-effect waves-light btn green modal-close cntxdnd_secundary">Cerrar</button>
			</div>
		</div>
	</body>
</html>